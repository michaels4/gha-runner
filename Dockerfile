FROM quay.io/evryfs/github-actions-runner:latest
USER root

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && mv ./kubectl /usr/local/sbin/kubectl \
    && chmod +x /usr/local/sbin/kubectl \
    && apt update \
    && apt -y install gettext-base \ 
    && apt clean

WORKDIR /home/runner
USER runner
ENTRYPOINT ["/entrypoint.sh"]
